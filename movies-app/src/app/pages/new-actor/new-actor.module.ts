import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewActorPageRoutingModule } from './new-actor-routing.module';

import { NewActorPage } from './new-actor-page/new-actor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    NewActorPageRoutingModule
  ],
  declarations: [NewActorPage]
})
export class NewActorPageModule {}
