import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewActorPage } from './new-actor-page/new-actor.page';

const routes: Routes = [
  {
    path: '',
    component: NewActorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewActorPageRoutingModule {}
