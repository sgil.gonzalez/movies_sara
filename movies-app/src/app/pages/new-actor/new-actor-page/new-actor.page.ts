import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ActorsService } from 'src/app/shared/services/actors.service';

@Component({
  selector: 'app-new-actor',
  templateUrl: './new-actor.page.html',
  styleUrls: ['./new-actor.page.scss'],
})
export class NewActorPage implements OnInit {
  actorForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private actorsService: ActorsService,
    private router: Router) { }

  ngOnInit() {
    this.actorForm = this.formBuilder.group({
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      bornCity:['', [Validators.required]],
      birthdate: ['', [Validators.required]],
      img: [''],
    });
  }

  resetForm(){
    this.actorForm.reset();
  }

  submitActorForm(){
    this.actorsService.addActor(this.actorForm.value).subscribe(
      () => {
        this.router.navigateByUrl('/actors');
      },
      (error) => {
        console.log('Ha ocurrido un error añadiendo el actor');
      }
    );
  }

}
