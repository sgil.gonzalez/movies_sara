import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Actor } from '../models/actor.model';

@Injectable({
  providedIn: 'root'
})
export class ActorsService {

  constructor(private http: HttpClient) { }

  getActors(): Observable<Actor[]>{
    return this.http.get<Actor[]>(`${environment.baseUrl}/actors`);
  }

  getActor(id: number): Observable<Actor> {
    return this.http.get<Actor>(`${environment.baseUrl}/actors/${id}`);
  }

  addActor(actor: Actor): Observable<Actor> {
    const newActor = { ...actor, rating: 0 };
    return this.http.post<Actor>(`${environment.baseUrl}/actors`, newActor);
  }

  updateActor(updateActor: Actor): Observable<Actor> {
    return this.http.put<Actor>(`${environment.baseUrl}/actors/${updateActor.id}`, updateActor);
  }

  removeActor(id: number): Observable<void> {
    return this.http.delete<void>(`${environment.baseUrl}/actors/${id}`);
  }

  voteUpActor(actor: Actor): Observable<Actor> {
    actor.rating++;
    return this.updateActor(actor);
  }

  voteDownActor(actor: Actor): Observable<Actor> {
    actor.rating--;
    return this.updateActor(actor);
  }
}
