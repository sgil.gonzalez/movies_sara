import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ActorCardComponent } from './component/actor-card/actor-card.component';
import { MovieCardComponent } from './component/movie-card/movie-card.component';
import { RatingComponent } from './component/rating/rating.component';



@NgModule({
  declarations: [MovieCardComponent, ActorCardComponent, RatingComponent],
  imports: [CommonModule, IonicModule],
  exports: [MovieCardComponent, ActorCardComponent, RatingComponent],
})
export class SharedModule { }
